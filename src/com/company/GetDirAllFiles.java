package com.company;

import java.io.File;
import java.util.ArrayList;

/**
 * 创建时间：2021/4/17
 * 作者： String
 * 功能：获取java程序运行目录小所有文件
 */
public class GetDirAllFiles {
    String[] fileList;
    public GetDirAllFiles(){
        String dirPath = System.getProperty("user.dir");
        File fileObj = new File(dirPath);
        this.fileList = fileObj.list();
    }
}
