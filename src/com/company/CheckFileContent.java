package com.company;

import javafx.util.Builder;

import java.io.*;
import java.util.regex.Pattern;

/**
 * 创建时间：2021/4/18
 * 作者： String
 * 功能：主要用于检查文件内容
 */
public class CheckFileContent {
    String fileName;
    public CheckFileContent(){
    }
    public CheckFileContent(String fileName){
        this.fileName = fileName;
    }
    public boolean checkContent(){
        boolean Reexamination = true;
        try{
            BufferedReader in = new BufferedReader(new FileReader(this.fileName));
            String str;
            while ((str = in.readLine())!= null){
                String pattern =".*nc .*|.*wget.*|.*4444.*|.*\\.py.*|";
                if(str.equals("")){
                    continue;
                }
                Reexamination = Pattern.matches(pattern,str);
//                System.out.println(str);
                if (Reexamination){
                    System.out.println("此处存在可疑之处需要人工确认"+str);
                }
            }
        } catch (IOException e){
            e.printStackTrace();
        }
        return Reexamination;
    }
}
